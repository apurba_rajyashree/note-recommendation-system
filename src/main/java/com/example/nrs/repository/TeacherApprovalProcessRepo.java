package com.example.nrs.repository;

import com.example.nrs.entity.Status;
import com.example.nrs.entity.TeacherApprovalProcess;
import com.example.nrs.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherApprovalProcessRepo extends JpaRepository<TeacherApprovalProcess,Integer> {

    List<TeacherApprovalProcess> findAllByStatus(Status status);

    TeacherApprovalProcess findByUserAndStatus(User user, Status status);

    List<TeacherApprovalProcess> findAllByUserAndStatus(User user,Status status);
}