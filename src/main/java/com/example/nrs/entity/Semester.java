package com.example.nrs.entity;

import com.example.nrs.dto.SemesterDto;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "semester", uniqueConstraints = {
        @UniqueConstraint(name = "uk_semster_department", columnNames = {"name", "department_id"})}
       )
@Where(clause = "is_active=true")
public class Semester {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="name",nullable = false)
    @Enumerated(EnumType.STRING)
    private SemesterName semesterName;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="department_id",referencedColumnName = "id")
    @JsonBackReference(value = "department")
    private Department department;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "semester")
    @JsonManagedReference(value = "semester")
    private List<Course> courseList;

    @Column(name = "is_active")
    private boolean isActive=Boolean.TRUE;

    public Semester(SemesterDto semesterDto){
        this.id=semesterDto.getId();
        this.semesterName=semesterDto.getSemesterName();
        this.department=semesterDto.getDepartment();
        this.isActive=semesterDto.isActive();
    }
}
