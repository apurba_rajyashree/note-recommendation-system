package com.example.nrs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteRecommendationSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteRecommendationSystemApplication.class, args);
    }

}
