package com.example.nrs.service;

import com.example.nrs.dto.DepartmentDto;

import java.util.List;

public interface DepartmentService {
    DepartmentDto createDepartment(DepartmentDto departmentDto);

    List<DepartmentDto> getAllDepartment();
    DepartmentDto getDepartmentByName(String name);

    String deleteDepartment(Integer id);

}
